﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public GameObject nonfpsModel;

    public GameObject playerUiPrefab;

    public PlayerMovementController playerMovementController;
    public Camera fpsCamera;

    private Animator animator;
    public Avatar fpsAvatar;
    public Avatar nonfpsAvatar;
    

    private Shooting shooting;

    [SerializeField]
    TextMeshProUGUI playerNameText;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        playerMovementController = this.GetComponent<PlayerMovementController>();
        fpsModel.SetActive(photonView.IsMine);
        nonfpsModel.SetActive(!photonView.IsMine);
        animator.SetBool("isLocalPlayer", photonView.IsMine);
        shooting = this.GetComponent<Shooting>();
        playerNameText.text = photonView.Owner.NickName;


        if (photonView.IsMine)
        {
            this.animator.avatar = fpsAvatar;
        }
        else
        {
            this.animator.avatar = nonfpsAvatar;
        }

        if(photonView.IsMine)
        {
            GameObject playerUi = Instantiate(playerUiPrefab);
            playerMovementController.fixedTouchField = playerUi.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerUi.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            fpsCamera.enabled = true;
            playerUi.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => shooting.Fire());
        


        }
        else
        {
            
            playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
}
