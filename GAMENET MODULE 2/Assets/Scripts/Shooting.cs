﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;


public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;
    public GameObject killedUIText;
    public int score;
    private int scoreLimit = 10;
  
    

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    public float health;
    public Image healthBar;



    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
        health = startHealth;
        healthBar.fillAmount = health / startHealth;

        animator = this.GetComponent<Animator>();
       
       
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            photonView.RPC("DisplayScore", RpcTarget.All);

        }
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray,out hit,200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered,25);
                if (photonView.IsMine)
                {
                    if(hit.collider.gameObject.GetComponent<Shooting>().health<=0)
                    {
                        photonView.RPC("AddScore", RpcTarget.All);
                    }
                    
                }

            }
            
        }
    }
    [PunRPC]
    public void TakeDamage(int damage,PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health<=0)
        {
            Die();
            GameObject playerUI = GameObject.Find("PlayerUI(Clone)");
            GameObject killedUI = Instantiate(killedUIText, new Vector3(-727, 245, 295), Quaternion.identity);
            killedUI.transform.SetParent(playerUI.transform, false);
            killedUI.GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;
            Destroy(killedUI, 2.0f);

        }
        
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine("RespawnCountdown");

        }
    }
    

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are Killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        int randomPointX = Random.Range(-20, 20);
        int randomPointZ = Random.Range(-20, 20);

        this.transform.position = GameManager.instance.RandomSpawnPoint();
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);

    }
    [PunRPC]
    public void RegainHealth()
    {
        
        health=100;
        healthBar.fillAmount = health / startHealth;
    }

    IEnumerator BackToLobby()
    {
        yield return new WaitForSeconds(4.5f);
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene(0);

    }
    [PunRPC]
    public void AddScore(PhotonMessageInfo info)
    {
        score++;
        if (score >= scoreLimit)
        {
            GameObject winText = GameObject.Find("Game Winner Text");
            transform.GetComponent<PlayerMovementController>().enabled = false;
            winText.GetComponent<Text>().text = info.Sender.NickName + " has Won the Game";
            StartCoroutine("BackToLobby");
        }
    }
    [PunRPC]
    public void DisplayScore()
    {
        GameObject scoreText = GameObject.Find("Score Text");
        scoreText.GetComponent<Text>().text = "Kills: "+score;
        
    }


  
    
}
