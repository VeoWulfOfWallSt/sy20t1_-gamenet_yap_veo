﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject playerPrefab;
    public Transform[] spawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        if (PhotonNetwork.IsConnectedAndReady)
        {
         
            PhotonNetwork.Instantiate(playerPrefab.name, RandomSpawnPoint(),Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 RandomSpawnPoint()
    {
        Vector3 position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
        return position;
    }

    
}
