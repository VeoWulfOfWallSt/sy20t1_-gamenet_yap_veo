﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerNameInputManager : MonoBehaviour
{
  public void SetPlayerName(string playerName)
   {
        PhotonNetwork.NickName = playerName;
   }
   
}
