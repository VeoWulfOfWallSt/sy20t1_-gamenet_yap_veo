﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Image healthbar;
    private float startHealth = 100;

    public float health;
    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
    }
    [PunRPC]
    public void TakeDmg(int damage)
    {
        health -= damage;

        healthbar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            if(photonView.IsMine)
            {
                GameManager.instance.LeaveRoom();
            }
           
        }
    }
    
}
