﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Eliminated : MonoBehaviourPunCallbacks
{
    public enum RaiseEventCode
    {
        WhoIsEliminatedEventCode = 0
    }
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.WhoIsEliminatedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string killed = (string)data[0];
            string killer = (string)data[1];
            int viewId = (int)data[2];
            GameObject killedUiText = DeathRaceManager.instance.killedUi;

            Debug.Log(killer + " has Killed " + killed);
            killedUiText.GetComponent<Text>().text = killed + " has been elimated";
            Destroy(killedUiText, 5);

            GameObject whoKilledWho = DeathRaceManager.instance.whoKilledWho;
            whoKilledWho.GetComponent<Text>().text = killer +" has killed "+killed;
            Destroy(whoKilledWho, 5);

            DeathRaceManager.instance.numberOfActivePlayers--;
            if (DeathRaceManager.instance.numberOfActivePlayers == 1)
            {
                Debug.Log(killer + " has won");
                GameObject Winner = DeathRaceManager.instance.Winner;


                Winner.GetComponent<Text>().text = killer + " has Won";
            }
            
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EliminatedPlayer(string killed, string killer)
    {
        //GetComponent<PlayerSetup>().camera.transform.parent = null;
        //GetComponent<VehicleMovement>().enabled = false;
        if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(0))
        {
            this.GetComponent<LaserCar>().enabled = false;

        }
        else if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(1))
        {
            this.GetComponent<LaserCar>().enabled = false;

        }
        else if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(2))
        {
            this.GetComponent<BulletCar>().enabled = false;
        }


        int viewId = photonView.ViewID;

        //event data
        object[] data = new object[] { killed, killer, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoIsEliminatedEventCode, data, raiseEventOptions, sendOptions);

    }

}
