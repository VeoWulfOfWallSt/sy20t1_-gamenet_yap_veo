﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public TextMeshProUGUI playerNameText;

    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }
        else if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {

            GetComponent<VehicleMovement>().isControlEnabled = true;

            if(PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(0))
            {
                this.GetComponent<LaserCar>().enabled = photonView.IsMine;
                
            }
            else if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(1))
            {
               this.GetComponent<LaserCar>().enabled = photonView.IsMine;
                
            }
            else if (PhotonNetwork.LocalPlayer.CustomProperties.ContainsValue(2))
            { 
                this.GetComponent<BulletCar>().enabled = photonView.IsMine;
            }
            
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            playerNameText.text = photonView.Owner.NickName;
        }
        
    }
}
    