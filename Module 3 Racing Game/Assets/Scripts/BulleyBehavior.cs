﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BulleyBehavior : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //this.GetComponent<Rigidbody>().AddForce(transform.forward * 20);

        photonView.RPC("Move", RpcTarget.AllBuffered);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            collision.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25f);
        }
      
        Destroy(this.gameObject);
    }
    [PunRPC]
    public void Move()
    {

        this.GetComponent<Rigidbody>().AddForce(transform.forward * 20);

    }


}
