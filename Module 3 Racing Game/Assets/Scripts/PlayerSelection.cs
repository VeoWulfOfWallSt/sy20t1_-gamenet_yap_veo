﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSelection : MonoBehaviour
{
    public GameObject[] RacingSelectablePlayers;
    public GameObject[] DeathRaceSelectablePlayers;


    public int playerSelectionNumber;
    // Start is called before the first frame update
    void Start()
    {
 
        playerSelectionNumber = 0;

        ActivatePlayer(playerSelectionNumber);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ActivatePlayer(int x)
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            foreach (GameObject go in RacingSelectablePlayers)
            {
                go.SetActive(false);
            }
            RacingSelectablePlayers[x].SetActive(true);
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            foreach (GameObject go in DeathRaceSelectablePlayers)
            {
                go.SetActive(false);

            }
            DeathRaceSelectablePlayers[x].SetActive(true);
        }
        

        

        //Setting Player Selection for vehicle
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_SELECTION_NUMBER, playerSelectionNumber } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);       
    }

    public void goToNextPlayer()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            playerSelectionNumber++;
            if (playerSelectionNumber >= RacingSelectablePlayers.Length)
            {
                playerSelectionNumber = 0;
            }

            ActivatePlayer(playerSelectionNumber);
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            playerSelectionNumber++;
            if (playerSelectionNumber >= DeathRaceSelectablePlayers.Length)
            {
                playerSelectionNumber = 0;
            }
        }
 
        ActivatePlayer(playerSelectionNumber);
    }
    public void goToPreviousPlayer()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            playerSelectionNumber--;

            if (playerSelectionNumber < 0)
            {
                playerSelectionNumber = RacingSelectablePlayers.Length - 1;
            }

            ActivatePlayer(playerSelectionNumber);
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            playerSelectionNumber--;

            if (playerSelectionNumber < 0)
            {
                playerSelectionNumber = DeathRaceSelectablePlayers.Length - 1;
            }
        }

        ActivatePlayer(playerSelectionNumber);
    }
}
