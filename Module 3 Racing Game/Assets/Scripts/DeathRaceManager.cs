﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceManager : MonoBehaviour
{

    public GameObject[] vechiclePrefabs;
    public Transform[] startingPostions;
    public int numberOfActivePlayers;
  
    public static DeathRaceManager instance;

    public Text timeText;
    public GameObject killedUi;
    public GameObject Winner;
    public GameObject whoKilledWho;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        numberOfActivePlayers = PhotonNetwork.CurrentRoom.PlayerCount;
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPostions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vechiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);

            }
        }
     
    }

   
}
