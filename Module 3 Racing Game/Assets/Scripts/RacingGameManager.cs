﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class RacingGameManager : MonoBehaviour
{
    public GameObject[] vechiclePrefabs;
    public Transform[] startingPostions;
    public GameObject[] finisherTextUi;

    public static RacingGameManager instance;

    public List<GameObject> lapTriggers = new List<GameObject>();

    public Text timeText;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;

        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPostions[actorNumber-1].position;
                PhotonNetwork.Instantiate(vechiclePrefabs[(int)playerSelectionNumber].name,instantiatePosition,Quaternion.identity);

            }
        }
        foreach (GameObject go in finisherTextUi)
        {
            go.SetActive(false);
        }
    }

    
}
