﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LaserCar : MonoBehaviourPunCallbacks
{
    public GameObject laser;
    public LineRenderer lr;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.photonView.RPC("FireLaser", RpcTarget.AllBuffered);
        }
       
    }
   
    IEnumerator DisableLaser()
    {
        yield return new WaitForSeconds(0.1f);
        lr.enabled = false;
    }
    [PunRPC]
    public void FireLaser()
    {
        lr.enabled = true;
        lr.SetPosition(0, laser.transform.position);
        RaycastHit hit;
        if (Physics.Raycast(laser.transform.position, transform.forward, out hit))
        {

            lr.SetPosition(1, hit.point);

            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25f);
               
            }
            //StartCoroutine("DisableLaser");

        }
        StartCoroutine("DisableLaser");
    }


   
   


}
