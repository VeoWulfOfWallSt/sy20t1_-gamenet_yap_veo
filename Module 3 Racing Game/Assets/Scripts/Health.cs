﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Photon.Pun;

public class Health : MonoBehaviourPunCallbacks
{
    [Header("HP Related Stuff")]
    public float startHealth = 100;
    public float health;
    public Image healthBar;
    public GameObject[] car;

    
    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [PunRPC]
    public void TakeDamage(float damage, PhotonMessageInfo info)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;
        Debug.Log(health / startHealth);
        if (health <= 0)
        {
            Debug.Log("Dead");

            this.GetComponent<Eliminated>().EliminatedPlayer(info.photonView.Owner.NickName, info.Sender.NickName);
            foreach (GameObject go in car)
            {
                go.SetActive(false);

            }




        }
    }

   
}
